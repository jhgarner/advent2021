{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE Strict #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Lib
  ( parser,
    partA,
    partB,
  )
where

import Control.Comonad
import Control.Zipper
import Data.Char (digitToInt, isLower)
import Data.Foldable hiding (product, toList)
import Data.Functor.Foldable (Corecursive (apo), ListF (Cons, Nil), Recursive (project), ana, cata, para, refold, unfold)
import Data.Functor.Foldable.TH
import Data.List (elemIndex, findIndex, intersect, nub, partition, (\\))
import Data.Semigroup (Max (Max))
import Data.Set
    (insert, member, union, delete, difference )
import Relude hiding (Down, exp, fromList, head, init, last, many, modify, some, tail, sum, map, toList)
import Relude.Extra.Lens
import Relude.Unsafe (fromJust, head, init, last, tail, (!!))
import Text.Megaparsec
import Text.Megaparsec.Char (alphaNumChar, asciiChar, char, digitChar, hspace, letterChar, lowerChar, newline, numberChar, punctuationChar, space, string)
import Text.Megaparsec.Char.Lexer (decimal)
import Data.Massiv.Array (Stencil, Ix2 ((:.)), makeStencil, Sz (Sz2), mapStencil, Border (Fill, Edge), makeArray, Comp (Seq), compute, map)
import Data.Massiv.Array.Manifest
import Control.Monad.Memo hiding (lookup, add)
import System.IO.Unsafe (unsafePerformIO)
import Control.Monad.Memo.Class hiding (lookup, add)
import Data.Map.Strict (fromListWith, toList, lookup, insert)
import Data.IntSet (toAscList, fromList)


type Problem = [(Bool, (Int, Int), (Int, Int), (Int, Int))]

num :: ParsecT Void Text IO Int
num = choice [char '-' *> fmap negate decimal, decimal]

parser :: ParsecT Void Text IO Problem
parser = flip sepEndBy newline do
  b <- choice [string "on " $> True, string "off " $> False]
  string "x="
  xs <- (,) <$> num <*> (string ".." *> num)
  string ",y="
  ys <- (,) <$> num <*> (string ".." *> num)
  string ",z="
  zs <- (,) <$> num <*> (string ".." *> num)
  pure (b, xs, ys, zs)

-- Didn't feel like naming part 1 helpers...
g :: Int -> Int -> [Int]
g a b
  | a < b = [a..b]
  | otherwise = [b..a]

f :: Set (Int, Int, Int) -> (Bool, (Int, Int), (Int, Int), (Int, Int)) -> IO (Set (Int, Int, Int))
f s (False, (x0, x1), (y0, y1), (z0, z1)) = do
  let xs = g x0 x1
      ys = g y0 y1
      zs = g z0 z1
      all = [(x, y, z) | x <- xs, y <- ys, z <- zs]
  pure $ if any (\a -> a < -50 || a > 50) [x0, x1, y0, y1, z0, z1] then s else foldl' (flip delete) s all
f s (True, (x0, x1), (y0, y1), (z0, z1)) = do
  let xs = g x0 x1
      ys = g y0 y1
      zs = g z0 z1
      all = [(x, y, z) | x <- xs, y <- ys, z <- zs]
  pure $ if any (\a -> a < -50 || a > 50) [x0, x1, y0, y1, z0, z1] then s else foldl' (flip Data.Set.insert) s all

partA :: Problem -> IO ()
partA input = do
  print "STARTING"
  foldlM f mempty input >>= print . length

type Point = (Int, Int, Int)
type Cube = (Point, Point)

isValid :: Cube -> Bool
isValid ((x0, y0, z0), (x1, y1, z1)) =
  x0 < x1 && y0 < y1 && z0 < z1

area :: Cube -> Int
area cube@((x0, y0, z0), (x1, y1, z1))
  | isValid cube = (x1-x0) * (y1-y0) * (z1-z0)
  | otherwise = 0

cut :: Cube -> Cube -> Cube
cut ((x0, y0, z0), (x1, y1, z1)) ((a0, b0, c0), (a1, b1, c1)) =
  ((max a0 x0, max b0 y0, max c0 z0), (min a1 x1, min b1 y1, min c1 z1))

-- Helpful debug function
traceLabel :: (Show a, Show b) => b -> a -> a
-- traceLabel label a = a -- traceShow (label, a) a
traceLabel label a =
  traceShow (label, a) a


-- Thanks https://stackoverflow.com/questions/69137352/computing-the-volume-of-the-union-of-axis-aligned-cubes
-- Converted the first python answer to Haskell
lengthUnion :: [(Int, Int)] -> Int
lengthUnion intervals =
  let events = sort $ intervals >>= \(l, u) -> [(l, 1), (u, -1)]
      (_, _, result) =
        foldl'
          ( \(previous, overlap, total) (x, delta) ->
              (x, overlap + delta, if overlap > 0 then total + x - previous else total)
          )
          (0, 0, 0)
          events
   in result

allBoundaries :: [(Int, Int)] -> [Int]
allBoundaries intervals =
  toAscList $ fromList $ intervals >>= \(l, u) -> [l, u]

subdivideAt :: (Int, Int) -> [Int] -> [(Int, Int)]
subdivideAt (lower, upper) boundaries =
  let bounded = takeWhile (< upper) $ dropWhile (< lower) $ sort boundaries
   in zip (lower:bounded) (bounded ++ [upper])

len :: (Int, Int) -> Int
len (l, u) = u - l

type Cube2 = ((Int, Int), (Int, Int), (Int, Int))

volumeUnion :: [Cube2] -> Int
volumeUnion cubes =
  let xBoundaries = allBoundaries [x | (x, _, _) <- cubes]
      yBoundaries = allBoundaries [y | (_, y, _) <- cubes]
      subProblems =
        cubes >>= \(cx, cy, cz) ->
          subdivideAt cx xBoundaries >>= \x ->
            subdivideAt cy yBoundaries <&> \y ->
              ((x, y), [cz])
   in sum [len x * len y * lengthUnion zIntervals | ((x, y), zIntervals) <- toList $ fromListWith (++) subProblems]

toCube2 :: Cube -> Cube2
toCube2 ((x0, y0, z0), (x1, y1, z1)) = ((x0, x1), (y0, y1), (z0, z1))

amountToRemove :: Cube -> [Cube] -> Int
amountToRemove c cs =
  let bounded = fmap toCube2 $ filter isValid $ fmap (cut c) cs
   in area c - volumeUnion bounded

addTogether :: [(Int, Cube)] -> [(Int, Cube)] -> IO Int
addTogether [] _ = pure 0
addTogether ((i, c):cs) offs = do
  print ("Adding", i)
  let justOffs = snd <$> filter (\(i', _) -> i' > i) offs
      turningOn = amountToRemove c $ justOffs ++ fmap snd cs
  turningRestOn <- addTogether cs offs
  pure $ turningOn + turningRestOn

mkCube :: (Bool, (Int, Int), (Int, Int), (Int, Int)) -> Cube
mkCube (_, (x0, x1), (y0, y1), (z0, z1)) =
  let l = (min x0 $ x1+1, min y0 $ y1+1, min z0 $ z1+1)
      r = (max x0 $ x1+1, max y0 $ y1+1, max z0 $ z1+1)
   in (l, r)

partB :: Problem -> IO ()
partB input = do
  let (ions, ioffs) = partition (\(_, (b, _, _, _)) -> b) $ zip [0..] input
      ons = fmap (second mkCube) ions
      offs = fmap (second mkCube) ioffs
  print "B"
  x <- addTogether ons offs
  print x
  -- foldlM f2 mempty input >>= print . length
