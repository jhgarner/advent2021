module Main where

import Relude
import Data.Text.IO as T
import Lib
import Text.Megaparsec (runParserT, errorBundlePretty)

main :: IO ()
main = do
  parsedE <- T.getContents >>= runParserT parser ""
  let parsed = either (error . toText . errorBundlePretty) id parsedE
  partA parsed
  partB parsed
