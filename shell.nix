let
  pkgs = import <nixpkgs> {};
in
pkgs.mkShell {
  buildInputs = [
    pkgs.stack
    pkgs.ghc
    pkgs.haskell-language-server
    pkgs.niv
    pkgs.cabal-install
  ];
}
